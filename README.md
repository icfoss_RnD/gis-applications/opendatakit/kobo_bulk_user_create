Steps for bulk user creation in kobotoolbox using django User model
 - Create a csv file (**usernames.csv**) with sl.no and usernames
 - Make a folder with name **create_users**
 - copy the files **create_user.py**, **__init__.py**,**usernames.csv**
 - Login to kpi docker container using **docker exec -ti <kpi_container_name> bash** and replicate the folder **create_users** inside container 
 - now run **python manage.py shell**
 - inside shell, insert **from create_users import create_user** and press enter
 - next line, insert **create_user.create_user()** and press enter; now the function will work and create users in the list (username from list,'password' as password)
