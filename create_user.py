def create_user():
 # content of "create_user.py" file
 import csv
 from django.contrib.auth import get_user_model

 # see ref. below
 UserModel = get_user_model()

 input_csv = 'create_users/usernames.csv'
 with open(input_csv) as input_file:
  reader = csv.reader(input_file)
  for row in reader:
   uid=row[0]
   uname=row[1]
   if not UserModel.objects.filter(username=uname).exists():
    user=UserModel.objects.create_user(uname, password='password')
    user.is_superuser=False
    user.is_staff=True
    user.save()
    print 'created user '+str(uid)+': '+uname+'\n'
 print 'completed!\n'

